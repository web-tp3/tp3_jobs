# TYPO3 Extension ``tp3_jobs``
[![Latest Stable Version](https://poser.pugx.org/web-tp3/tp3jobs/v/stable)](https://packagist.org/packages/web-tp3/tp3jobs)
[![Daily Downloads](https://poser.pugx.org/web-tp3/tp3jobs/d/daily)](https://packagist.org/packages/web-tp3/tp3jobs)
[![Total Downloads](https://poser.pugx.org/web-tp3/tp3jobs/downloads)](https://packagist.org/packages/web-tp3/tp3jobs)
[![License](https://poser.pugx.org/web-tp3/tp3_jobs/license)](https://packagist.org/packages/web-tp3/tp3_jobs)

Display Jobs on you site. Microdate for the searchangines and compatibility 


##install
 
The recommended way to install the extension is by using (Composer)
[1]. In your Composer based TYPO3 project root, just do `composer require web-tp3/tp3jobs`. 

##Manual
[Manual](https://web.tp3.de/manual/tp3jobs.html)


##Copyleft
This file is part of the "tp3jobs" Extension for TYPO3 CMS.
Based on the ext:cs_joboffers 


[Manual](https://web.tp3.de/manual/tp3jobs.html)

[Repository](https://bitbucket.org/web-tp3/tp3_jobs)
